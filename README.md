
# Simple Spring-Boot/Maven

## run local
```
mvn spring-boot:run
```

## package and run in production
```
mvn package
java -jar target/firstmaven-x.y.z-SNAPSHOT.jar
```
Where x, y and z represent the release version

## View the site localy

By visiting: `http://localhost:8080/`.


## GitLab CI for review and production deployment

CI pipeline with automated build, testing, reporting and review deployments for all branches.

Manually triggered automated deployment to AWS from master branch triggered on commits to master.


## Tutorials
https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-using-springbootapplication-annotation.html
https://github.com/spring-projects/spring-boot/blob/master/spring-boot-project/spring-boot-starters/README.adoc
https://spring.io/guides/
https://docs.spring.io/spring/docs/5.0.6.RELEASE/spring-framework-reference/web.html#mvc
