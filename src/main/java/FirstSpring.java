package org.puzzleduck.first;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.*;

@RestController
@EnableAutoConfiguration
public class FirstSpring {

  @RequestMapping("/")
  String home() {
    return "First Maven Spring Boot";
  }

  public static void main(String[] args) throws Exception{
    SpringApplication.run(FirstSpring.class, args);
  }
}
